var express = require('express');
var controller = require('../api/user/user.controller');
var router = express.Router();


/* GET users listing. */

router.get('/user/:username', controller.getAccount);

router.post('/findUser', controller.search);

router.post('/register', controller.createAccount);

router.post('/user/:username', controller.updateAccount);

router.post('/logout', controller.logout);

router.post('/login', controller.login);

module.exports = router;
