var express    = require('express');
var controller = require('../api/item/item.controller');
var router = express.Router();

/* GET articles page. */

router.get('/', controller.getAll);

router.get('/filter', controller.filter);

router.post('/', controller.create);

router.get('/:id', controller.getOne);

router.put('/:id', controller.updateItem);

router.delete('/:id', controller.delete);


module.exports = router;
