var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ItemSchema = new Schema({
    title: { type: String, required: true },
    price: { type: Number, required: true },
    description: { type: String, required: true },
    images: { type: String, required: true },
    ownerId: { type: String, required: true }
});
module.exports = mongoose.model('Item', ItemSchema);