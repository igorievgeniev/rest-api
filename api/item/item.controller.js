var Item = require('./item.model');
var User = require('../user/user.model');

exports.getAll = function(req, res) {
    var accessToken = req.cookies.accessToken || '';
    var currentUser = {};
    User.findOne({accessToken: accessToken}, function(err, user){
        if(err) {
            return res.status(500).json(err);
        }
        currentUser = user || {username: ''};

        var perPage = 6,
            page = Math.max(1, req.query.page || 1)-1;

        Item.find().limit(perPage).skip(perPage * page).exec(function(err, items){
            if(err) {
                return res.status(500).json(err);
            }
            Item.count(function(err, count){
                if(err) {
                    return res.status(500).json(err);
                }
                var url = '?';
                var isNext = (count-(perPage * page) > perPage);
                return res.render('index', {items: items, page: page+1, isNext: isNext, url: url, count: count, username: currentUser.username })
            });
        });
    });
}
exports.create = function(req, res) {
    var accessToken = req.cookies.accessToken;
    var currentUser = {};
    if(!accessToken) {
        return res.status(500);
    }
    User.findOne({accessToken: accessToken}, function(err, user) {
        if (err) {
            return res.status(500).json(err);
        }
        currentUser = user || {username: ''};
        req.body.ownerId = currentUser._id;
        Item.create(req.body, function (err, item) {
            if (err) {
                return res.status(500).json(err);
            }
            return res.status(201).json(item);
        });
    });
}
exports.getOne = function(req, res) {
    var accessToken = req.cookies.accessToken || '';
    var currentUser = {};
    User.findOne({accessToken: accessToken}, function(err, user){
        if(err) {
            return res.status(500).json(err);
        }
        currentUser = user || {username: ''};
        Item.findById(req.params.id, function(err, item){
            if(err) {
                return res.status(500).json(err);
            }
            if(!item) {
                return res.status(404).json('item not found');
            }
            item.username = currentUser.username;
            item.userId = currentUser._id;
            return res.render('item', item);
        })
    });
}
exports.updateItem = function(req, res) {
    Item.findById(req.params.id, function(err, item){
        if(err) {
            return res.status(500).json(err);
        }
        if(!item) {
            return res.status(404).json('item not found');
        }
        if(req.body.title) {
            item.title = req.body.title;
        }
        if(req.body.price) {
            item.price = req.body.price;
        }
        if(req.body.description) {
            item.description = req.body.description;
        }
        if(req.body.images) {
            item.images = req.body.images;
        }
        item.save(function(err, item){
            if(err) {
                return res.status(500).json(err);
            }
            return res.status(200).json(item);
        });
    })
}
exports.delete = function(req, res) {
    Item.findById(req.params.id, function(err, item){
        if(err) {
            return res.status(500).json(err);
        }
        if(!item) {
            return res.status(404).json('item not found');
        }
        item.remove(function(err){
            if(err) {
                return res.status(500).json(err);
            }
            return res.status(200).send('item deleted');
        });
    })
}
exports.filter = function(req, res) {
    var accessToken = req.cookies.accessToken || '';
    var currentUser = {};
    User.findOne({accessToken: accessToken}, function(err, user){
        if(err) {
            return res.status(500).json(err);
        }
        currentUser = user || {username: ''};

        var getTitle = (req.query.title) ? {title: {$regex: req.query.title, $options: '$i'}} : {};
        var getSort = (req.query.sort) ? {price: req.query.sort} : {};
        var perPage = 6
            , page = Math.max(1, req.query.page || 1) -1;
        Item.find(getTitle).sort(getSort).limit(perPage).skip(perPage * page).exec(function (err, items) {
            if (err) {
                return res.status(500).json(err);
            }
            Item.find(getTitle).sort(getSort).exec(function(err, count){
                if(err) {
                    return res.status(500).json(err);
                }
                count = count.length;
                var isNext = (count-(perPage * page) > perPage);
                var url = '/filter?sort=' + req.query.sort + '&title=' + req.query.title + '&';
                return res.render('index', {items: items, page: page +1, isNext: isNext, url: url, count: count, username: currentUser.username})
            });
        });
    });
}