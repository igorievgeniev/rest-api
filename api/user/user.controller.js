var User = require('./user.model');
var Item = require('../item/item.model');


function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4()+s4()+s4()+s4()+s4()+s4()+s4()+s4();
}

exports.getAccount = function(req, res) {
    var accessToken = req.cookies.accessToken;
    if(accessToken == undefined || accessToken == null) {
        return res.status(403).send('login please');
    }
    User.findOne({accessToken: accessToken}, function(err, user){
        if (err) {
            return res.status(500).json(err);
        }
        if (!user) {
            return res.status(404).send('user is not found');
        }
        Item.find({ownerId: user._id}).exec(function(err, items){
            user.items = items;
            res.clearCookie('accessToken',{path: '/'});
            res.cookie('accessToken', accessToken, { maxAge: 120000, httpOnly: true });
            return res.render('account', user);
        });
    })
}


exports.createAccount = function(req, res) {
    req.body.accessToken = guid();
    User.create(req.body, function(err, user) {
        if(err) {
            return res.status(500).json(err);
        }
        Item.find({ownerId: user._id}).exec(function(err, items){
            user.items = items;
            res.cookie('accessToken', req.body.accessToken, { maxAge: 240000, httpOnly: true });
            return res.render('account', user);
        });
    });
}

exports.updateAccount = function(req, res) {
    var accessToken = req.cookies.accessToken || '';
    User.findOne({accessToken: accessToken}, function(err, user){
        if(err) {
            return res.status(500).json(err);
        }
        if(!user) {
            return res.status(404).json('item not found');
        }
        if(req.body.username) {
            user.username = req.body.username;
        }
        if(req.body.password && user.password == req.body.oldPassword) {
            user.password = req.body.password;
        }
        user.save(function(err, user){
            if(err) {
                return res.status(500).json(err);
            }
            return res.status(200).json(user);
        });
    })
}

exports.login = function(req, res) {
    User.findOne({username: req.body.username, password: req.body.password}, function(err, user){
        if(err) {
            return res.status(500).json(err);
        }
        if(!user) {
            return res.status(404).json('user not found');
        }
        var accessToken = guid();
        user.accessToken = accessToken;
        user.save();
        Item.find({ownerId: user._id}).exec(function(err, items){
            user.items = items;
            res.cookie('accessToken', accessToken, { maxAge: 120000, httpOnly: true });
            return res.render('account', user);
        });
    })
}

exports.logout = function(req, res) {
	var accessToken = req.cookies.accessToken || '';
   User.findOne({accessToken: accessToken}, function(err, user){
        if(err) {
            return res.status(500).json(err);
        }
        if(!user) {
            return res.status(404).json('item not found');
        }
        user.accessToken = null;
        user.save();
        res.clearCookie('accessToken',{path: '/'});
        return res.send('logged out');
    })
}

exports.search = function(req, res) {
    var accessToken = req.cookies.accessToken;
    if(accessToken == undefined || accessToken == null) {
        return res.status(403).send('login please');
    }
    User.findOne({accessToken: accessToken}).exec(function (err, user){
        if (err) {
            return res.status(500).json(err);
        }
        User.findOne({username: req.body.username}).exec(function (err, searchUser) {
            if (err) {
                return res.status(500).json(err);
            }
            Item.find({ownerId: searchUser._id}).exec(function(err, items){
                searchUser.items = items;
                return res.render('user-page', {searchUser: searchUser.username, username: user.username, items: searchUser.items});
            });
        })
    })
}
