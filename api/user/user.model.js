var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    accessToken: {
        type: String,
        unique: true,
        required: false
    }
});

//UserSchema.set('validateBeforeSave', true);
//UserSchema.path('password').validate(function (value) {
//    return value.length > 4;
//});



module.exports = mongoose.model('User', UserSchema);