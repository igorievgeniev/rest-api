$(document).ready(function(){
    //select styles
    $('select').each(function(){
        $(this).siblings('span').text( $(this).children('option:selected').text() );
    });
    $('select').change(function(){
        $(this).siblings('span').text( $(this).children('option:selected').text() );
    });

    // underscore call
    _.templateSettings = {
        interpolate : /\{\{(.+?)\}\}/g
    };

    //popups show
    function popupShow (a) {
        a.before('<div class="popup-overlay"></div>');
        a.fadeIn(100);
        $('.popup-overlay').click(function popupHide (){
            $(this).fadeOut(100);
            setTimeout(function(){
                $('.popup-overlay').remove();
            }, 100);
            a.fadeOut(100);
        })
    }
    function textPopupShow (text) {
        $('#content').append('<div class="text-popup">' + text + '</div>');
        $('.text-popup').before('<div class="popup-overlay"></div>');
        $('.popup-overlay').click(function popupHide (){
            $(this).fadeOut(100);
            setTimeout(function(){
                $('.popup-overlay').remove();
            }, 100);
            $('.text-popup').fadeOut(100);
        })
    }

    //login popup call
    $('.login-btn').on('click', function(){
        popupShow ($('#header .login-popup'));
    });

    //registration popup call
    $('.reg-btn').on('click', function(){
        popupShow ($('#header .reg-popup'));
    });

    // add menu call
    $('.add-button').on('click', function(){
        popupShow ($('.change-menu'));
        var form = $('.change-menu form');
        $('#add').on('click', function(e) {
            e.preventDefault();
            var errors = 0;
            $('#add-item :input').not('button').map(function(){
                if( !$(this).val() ) {
                    $(this).addClass('warning');
                    errors++;
                } else if ($(this).val()) {
                    $(this).removeClass('warning');
                }
            });
            if(errors > 0){
                $('.error').text("Please fill in the fields").fadeIn();
                return false;
            }
            var getItemName = $('#item-name').val();
            var getItemPrice = $('#item-price').val();
            var getItemDescription = $('#item-description').val();
            var getItemImage = $('#item-image').val();
            var newItem = {
                title: getItemName,
                price: getItemPrice,
                description: getItemDescription,
                images: getItemImage
            };
            $.ajax({
                url: '/',
                method: 'POST',
                dataType: 'json',
                data: newItem,
                complete: function() {
                    function popupHide (a){
                        $(this).fadeOut(100);
                        setTimeout(function(){
                            $('.popup-overlay').remove();
                        }, 100);
                        a.fadeOut(100);
                    };
                    popupHide ($('.change-menu'));
                    setTimeout(function(){
                        textPopupShow ('added');
                    }, 200);
                    var template = _.template($('#items-template').html());
                    var itemTempl = template({item: newItem});
                    $('.main-item-list').append(itemTempl);
                    $('#add-item').trigger('reset');
                    $('#add-item .error').text('');
                }
            });
        });
    });

    //change menu call
    $('.change-button').on('click', function(){
        popupShow ($('.change-menu'));
        $('#change').on('click', function(e) {
            e.preventDefault();
            var getItemName = $('#item-name').val();
            var getItemPrice = $('#item-price').val();
            var getItemDescription = $('#item-description').val();
            var getItemImage = $('#item-image').val();
            $.ajax({
                url: location.href,
                method: 'PUT',
                data: {
                    title: getItemName,
                    price: getItemPrice,
                    description: getItemDescription,
                    images: getItemImage
                },
                complete: function() {
                    function popupHide (a){
                        $(this).fadeOut(100);
                        setTimeout(function(){
                            $('.popup-overlay').remove();
                        }, 100);
                        a.fadeOut(100);
                    };
                    popupHide ($('.change-menu'));
                    setTimeout(function(){
                        textPopupShow ('changed');
                    }, 200);
                }
            });
        });
    });

    // delete item call
    $('.delete-button').on('click', function(){
        $.ajax({
            url: location.href,
            method: 'DELETE',
            complete: function() {
                textPopupShow ('deleted');
                window.location.href = '/'
            }
        });
    });

    //sort
    $('#sort select').on('change', function() {
        $('#sort').submit();
    });

    //sort & search queries
    var getFilterUrl = location.search.slice(1).split('&');
    for (var i = 0; i < getFilterUrl.length; i++) {
        var qurrentQuery = getFilterUrl[i].split('=');
        if (qurrentQuery[0] == 'sort'){
            $('#sort select').val(qurrentQuery[1]);
            $('select').each(function(){
                $(this).siblings('span').text( $(this).children('option:selected').text() );
            });
        } else if (qurrentQuery[0] == 'title') {
            $('#sort input[name="title"]').val(qurrentQuery[1]);
        }
    }

    //logout
    $('.logout-btn').on('click', function(e){
        e.preventDefault();
        $.ajax({
            url: '/account/logout/',
            type: 'POST',
            success: function(response) {
                textPopupShow(response);
                setTimeout(function(){
                    window.location.replace('/');
                }, 500);
            }
        });
    });

    //find user
    //$('#findUser').on('submit', function(e) {
    //    e.preventDefault();
    //    $.ajax({
    //        url: '/account/findUser',
    //        type: 'POST',
    //        data: $(this).serialize(),
    //        success: function(response){
    //            console.log(response)
    //        }
    //    })
    //})

});